// get data from the json server
const getData = async () => {
  try {
    const table = document.querySelector("#users-list");
    const res = await axios
      .get("http://localhost:3000/notes")
      .then((response) => {
        const notes = response.data;
        notes.map((note) => {
          // create a `tr` element
          const tr = document.createElement("tr");
          // create ID `td`
          const idTd = document.createElement("td");
          idTd.textContent = note.id;
          // create Name `td`
          const nameTd = document.createElement("td");
          nameTd.textContent = `${note.content}`;
          // create Name `td`
          const importantTd = document.createElement("td");
          importantTd.textContent = `${note.important}`;
          // add tds to tr
          tr.appendChild(idTd);
          tr.appendChild(nameTd);
          tr.appendChild(importantTd);
          // app tr to table
          table.querySelector("tbody").appendChild(tr);
        });
        console.log(notes);
      });
  } catch (error) {
    console.log(error);
  }
};
getData();

// post data to the json server
const postData = async (notes) => {
  try {
    const res = await axios.post("http://localhost:3000/notes", notes, {
      headers: {
        "Content-Type": "application/json",
      },
    });
  } catch (error) {
    console.log(error.response.data);
  }
};

const notes = {
  id: 9,
  content: "new notes",
  date: new Date(),
  important: false,
};

postData(notes);
