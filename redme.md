Use the package manager [yarn](https://www.npmjs.com/package/yarn) to execute this project.

```bash
yarn install -g yarn
```

## Run Locally

Download project

```bash
  git clone https://gitlab.com/familaghash/api.git
```

Go to the project directory

```bash
  cd api
```

Install dependencies

```bash
  yarn install
```

Start the server

```bash
  yarn json-server --watch db.json
```

## API Reference

#### Get all items

```http
  GET http://localhost:3000/notes
```

#### Get item

```http
  GET http://localhost:3000/notes${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Id of item to fetch |
